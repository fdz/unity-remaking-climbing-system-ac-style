﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	[System.Serializable]
	public class ClimbPointNeighbour
	{
		public enum TransitionStyle { WaitForInput, Automatic }
		public ClimbPoint point;
		public Vector2 direction;
		public TransitionStyle transitionStyle;

		public ClimbPointNeighbour(Vector2 direction)
		{
			this.direction = direction;
		}
	}

	public class ClimbPoint : MonoBehaviour
	{
		public bool isDismount;

		public Transform leftHand { get; private set; }
		public Transform leftElbow { get; private set; }
		public Transform leftFoot { get; private set; }
		public Transform leftKnee { get; private set; }

		public Transform rightHand { get; private set; }
		public Transform rightElbow { get; private set; }
		public Transform rightFoot { get; private set; }
		public Transform rightKnee { get; private set; }

		public Transform body { get; private set; }

		[SerializeField] ClimbPointNeighbour[] neighbours = {
			new ClimbPointNeighbour(new Vector2(0, 1)),
			new ClimbPointNeighbour(new Vector2(1, 1)),
			new ClimbPointNeighbour(new Vector2(1, 0)),
			new ClimbPointNeighbour(new Vector2(0, -1)),
			new ClimbPointNeighbour(new Vector2(1, -1)),
			new ClimbPointNeighbour(new Vector2(0, -1)),
			new ClimbPointNeighbour(new Vector2(-1, -1)),
			new ClimbPointNeighbour(new Vector2(-1, 0)),
			new ClimbPointNeighbour(new Vector2(-1, 1)),
		};

		private void Awake()
		{
			leftHand = transform.Find("LeftHandIK");
			leftElbow = transform.Find("LeftElbowIK");
			leftFoot = transform.Find("LeftFootIK");
			leftKnee = transform.Find("LeftKneeIK");

			rightHand = transform.Find("RightHandIK");
			rightElbow = transform.Find("RightElbowIK");
			rightFoot = transform.Find("RightFootIK");
			rightKnee = transform.Find("RightKneeIK");

			body = transform.Find("Body");
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.green;
			if (neighbours != null)
			{
				foreach (var n in neighbours)
				{
					if (n != null && n.point != null)
					{
						Gizmos.DrawLine(transform.position, n.point.transform.position);
					}
				}
			}
		}

		public ClimbPointNeighbour GetNeighbour(Vector2 direction)
		{
			return System.Array.Find(neighbours, n => n != null && n.point != null && n.direction == direction);
		}
	} // end of class
} // end of namespace