﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Climbing : MonoBehaviour
	{
		public enum State { None, FoundPoint, Mounting, Idle, DecidingMovement, MovingToPrevious, MovingToNext, Dismounting }

		ClimbPoint curPoint;
		public Vector3 transformOffset;
		public float weightSpeed = 5f;
		public float minDistance = 1f;
		public float mountingMultiplier = 1f;
		[Space]
		public float decisionStateDuration = .6f;
		[Space]
		public float headSpeed = 9f;
		[Space]
		public float handDelay = .15f;
		public float handSpeed = 9f;
		[Space]
		public float footDelay = .15f;
		public float footSpeed = 9f;
		[Space]
		public float bodyDelay = .15f;
		public float bodySpeed = 9f;
		public float bodyExtraAngle = 25f;
		public float bodyPercentageUp = .8f;
		public float bodyPercentageDown = .2f;
		public float bodyPercentageHorizontal = .35f;

		// [Header("LOOK ONLY")]
		public State state { get; private set; }
		ClimbPointNeighbour next;
		float curWeight = 0f;
		float decisionTimer = 0f;
		float footTimer = 0f;
		float handTimer = 0f;
		float bodyTimer = 0f;
		bool endDismount;

		Animator animator;

		Vector3 leftHandPosition;
		Quaternion leftHandRotation;
		Vector3 leftElbowPosition;
		Vector3 leftFootPosition;
		Quaternion leftFootRotation;
		Vector3 leftKneePosition;
		Vector3 rightHandPosition;
		Quaternion rightHandRotation;
		Vector3 rightElbowPosition;
		Vector3 rightFootPosition;
		Quaternion rightFootRotation;
		Vector3 rightKneePosition;
		Vector3 bodyPosition;
		Quaternion bodyRotation;
		Vector3 parentPosition;
		Vector3 parentForward;
		Quaternion parentRotation;
		Vector3 lookAtPosition;

		private void Awake()
		{
			animator = GetComponent<Animator>();
		}

		public void Begin()
		{
			if (curPoint == null)
			{
				var points = (ClimbPoint[])FindObjectsOfType(typeof(ClimbPoint));
				ClimbPoint nearest = null;
				float nearestDistance = Mathf.Infinity;
				foreach (var point in points)
				{
					// Debug.Log((transform.parent.position - (point.transform.position - point.transform.rotation * transformOffset)).magnitude + " <=? " + transformOffset.magnitude);
					// if ((transform.parent.position - (point.transform.position - point.transform.rotation * transformOffset)).magnitude <= transformOffset.magnitude)
					if (!point.isDismount)
					{
						float distance = (transform.parent.position - point.transform.position).magnitude;
						if (distance <= minDistance && (nearest == null || distance < nearestDistance))
						{
							curPoint = point;
							state = State.FoundPoint;
							nearestDistance = distance;
						}
					}
				}
			}
		}

		public void Drop()
		{
			if (state != State.Dismounting)
			{
				curPoint = null;
				state = State.None;
			}
		}

		public void EndDismount()
		{
			endDismount = true;
		}

		private void OnAnimatorIK(int layerIndex)
		{
			if (curPoint == null)
			{
				return;
			}

			curWeight = Mathf.Lerp(curWeight, 1f, Time.deltaTime * weightSpeed);

			var direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

			var handTime = Time.deltaTime * handSpeed;
			var bodyTime = Time.deltaTime * bodySpeed;
			var footTime = Time.deltaTime * footSpeed;
			var headTime = Time.deltaTime * headSpeed;

			var bodyPercentage = .5f;
			if (next != null)
			{
				if (next.direction.y > 0)
				{
					bodyPercentage = bodyPercentageUp;
				}
				else if (next.direction.y < 0)
				{
					bodyPercentage = bodyPercentageDown;
				}
				else
				{
					bodyPercentage = bodyPercentageHorizontal;
				}
			}

			if (state == State.FoundPoint)
			{
				state = State.Mounting;

				leftHandPosition   = animator.GetIKPosition    (AvatarIKGoal.LeftHand);
				leftHandRotation   = animator.GetIKRotation    (AvatarIKGoal.LeftHand);
				leftElbowPosition  = animator.GetIKHintPosition(AvatarIKHint.LeftElbow);

				rightHandPosition  = animator.GetIKPosition    (AvatarIKGoal.RightHand);
				rightHandRotation  = animator.GetIKRotation    (AvatarIKGoal.RightHand);
				rightElbowPosition = animator.GetIKHintPosition(AvatarIKHint.RightElbow);

				leftFootPosition   = animator.GetIKPosition    (AvatarIKGoal.LeftFoot);
				leftFootRotation   = animator.GetIKRotation    (AvatarIKGoal.LeftFoot);
				leftKneePosition   = animator.GetIKHintPosition(AvatarIKHint.LeftKnee);

				rightFootPosition   = animator.GetIKPosition    (AvatarIKGoal.RightFoot);
				rightFootRotation   = animator.GetIKRotation    (AvatarIKGoal.RightFoot);
				rightKneePosition   = animator.GetIKHintPosition(AvatarIKHint.RightKnee);

				bodyPosition = animator.bodyPosition;
				bodyRotation = animator.bodyRotation;

				lookAtPosition = curPoint.transform.position;
			}

			if (state == State.Mounting)
			{
				leftHandPosition = Vector3.Lerp(leftHandPosition, curPoint.leftHand.position, handTime * mountingMultiplier);
				leftHandRotation = Quaternion.Lerp(leftHandRotation, curPoint.leftHand.rotation, handTime * mountingMultiplier);
				leftElbowPosition = Vector3.Lerp(leftElbowPosition, curPoint.leftElbow.position, handTime * mountingMultiplier);

				rightHandPosition = Vector3.Lerp(rightHandPosition, curPoint.rightHand.position, handTime * mountingMultiplier);
				rightHandRotation = Quaternion.Lerp(rightHandRotation, curPoint.rightHand.rotation, handTime * mountingMultiplier);
				rightElbowPosition = Vector3.Lerp(rightElbowPosition, curPoint.rightElbow.position, handTime * mountingMultiplier);

				leftFootPosition = Vector3.Lerp(leftFootPosition, curPoint.leftFoot.position, footTime * mountingMultiplier);
				leftFootRotation = Quaternion.Lerp(leftFootRotation, curPoint.leftFoot.rotation, footTime * mountingMultiplier);
				leftKneePosition = Vector3.Lerp(leftKneePosition, curPoint.leftKnee.position, footTime * mountingMultiplier);

				rightFootPosition = Vector3.Lerp(rightFootPosition, curPoint.rightFoot.position, footTime * mountingMultiplier);
				rightFootRotation = Quaternion.Lerp(rightFootRotation, curPoint.rightFoot.rotation, footTime * mountingMultiplier);
				rightKneePosition = Vector3.Lerp(rightKneePosition, curPoint.rightKnee.position, footTime * mountingMultiplier);

				bodyPosition = Vector3.Lerp(bodyPosition, curPoint.body.position, bodyTime * mountingMultiplier);
				bodyRotation = Quaternion.Lerp(bodyRotation, curPoint.body.rotation, bodyTime * mountingMultiplier);

				var target = curPoint.transform.position - curPoint.transform.rotation * transformOffset;
				transform.parent.position = Vector3.Lerp(transform.parent.position, target, bodyTime * mountingMultiplier);
				transform.parent.forward = Vector3.Lerp(transform.parent.forward, curPoint.transform.forward, bodyTime * mountingMultiplier);
				
				animator.SetIKLimb(AvatarIKGoal.LeftFoot, curWeight, leftFootPosition, leftFootRotation, leftKneePosition);
				animator.SetIKLimb(AvatarIKGoal.RightFoot, curWeight, rightFootPosition, rightFootRotation, rightKneePosition);
				animator.SetIKLimb(AvatarIKGoal.RightHand, curWeight, rightHandPosition, rightHandRotation, rightElbowPosition);
				animator.SetIKLimb(AvatarIKGoal.LeftHand, curWeight, leftHandPosition, leftHandRotation, leftElbowPosition);

				animator.bodyPosition = bodyPosition;
				animator.bodyRotation = bodyRotation;

				animator.SetLookAtWeight(curWeight);
				lookAtPosition = Vector3.Lerp(lookAtPosition, curPoint.transform.position, headTime * mountingMultiplier);
				animator.SetLookAtPosition(lookAtPosition);

				if ((transform.parent.position - target).magnitude < .01f)
				{
					state = State.Idle;
					decisionTimer = decisionStateDuration;
				}
			}
			else if (state == State.Idle)
			{
				// Debug.Log(direction);
				decisionTimer = Mathf.Clamp(decisionTimer - Time.deltaTime, 0, decisionTimer);
				
				footTimer = Mathf.Clamp(footTimer - Time.deltaTime, 0, footTimer);
				bodyTimer = Mathf.Clamp(bodyTimer - Time.deltaTime, 0, bodyTimer);
				handTimer = Mathf.Clamp(handTimer - Time.deltaTime, 0, handTimer);

				if (handTimer <= 0)
				{
					leftHandPosition = Vector3.Lerp(leftHandPosition, curPoint.leftHand.position, handTime);
					leftHandRotation = Quaternion.Lerp(leftHandRotation, curPoint.leftHand.rotation, handTime);
					leftElbowPosition = Vector3.Lerp(leftElbowPosition, curPoint.leftElbow.position, handTime);

					rightHandPosition = Vector3.Lerp(rightHandPosition, curPoint.rightHand.position, handTime);
					rightHandRotation = Quaternion.Lerp(rightHandRotation, curPoint.rightHand.rotation, handTime);
					rightElbowPosition = Vector3.Lerp(rightElbowPosition, curPoint.rightElbow.position, handTime);
				}

				if (footTimer <= 0)
				{
					leftFootPosition = Vector3.Lerp(leftFootPosition, curPoint.leftFoot.position, footTime);
					leftFootRotation = Quaternion.Lerp(leftFootRotation, curPoint.leftFoot.rotation, footTime);
					leftKneePosition = Vector3.Lerp(leftKneePosition, curPoint.leftKnee.position, footTime);

					rightFootPosition = Vector3.Lerp(rightFootPosition, curPoint.rightFoot.position, footTime);
					rightFootRotation = Quaternion.Lerp(rightFootRotation, curPoint.rightFoot.rotation, footTime);
					rightKneePosition = Vector3.Lerp(rightKneePosition, curPoint.rightKnee.position, footTime);
				}

				if (bodyTimer <= 0)
				{
					bodyPosition = Vector3.Lerp(bodyPosition, curPoint.body.position, bodyTime);
					bodyRotation = Quaternion.Lerp(bodyRotation, curPoint.body.rotation, bodyTime);

					transform.parent.position = Vector3.Lerp(transform.parent.position, curPoint.transform.position - curPoint.transform.rotation * transformOffset, bodyTime);
					transform.parent.forward = Vector3.Lerp(transform.parent.forward, curPoint.transform.forward, bodyTime);
				}
				
				animator.SetIKLimb(AvatarIKGoal.LeftFoot, curWeight, leftFootPosition, leftFootRotation, leftKneePosition);
				animator.SetIKLimb(AvatarIKGoal.RightFoot, curWeight, rightFootPosition, rightFootRotation, rightKneePosition);
				animator.SetIKLimb(AvatarIKGoal.RightHand, curWeight, rightHandPosition, rightHandRotation, rightElbowPosition);
				animator.SetIKLimb(AvatarIKGoal.LeftHand, curWeight, leftHandPosition, leftHandRotation, leftElbowPosition);

				animator.bodyPosition = bodyPosition;
				animator.bodyRotation = bodyRotation;

				animator.SetLookAtWeight(curWeight);
				lookAtPosition = Vector3.Lerp(lookAtPosition, curPoint.transform.position, headTime);
				animator.SetLookAtPosition(lookAtPosition);
				
				next = curPoint.GetNeighbour(direction);
				if (next != null && decisionTimer <= 0)
				{
					Debug.Log("next " + next.point.name);
					state = State.DecidingMovement;
					footTimer = footDelay;
					bodyTimer = bodyDelay;
					handTimer = handDelay;
					// decisionTimer = footTimer + bodyTimer + handTimer;
					decisionTimer = decisionStateDuration;
					if (next.point.isDismount)
					{
						state = State.Dismounting;
						endDismount = false;
						animator.SetTrigger("dismount");
					}
				}
			}
			else if (state == State.DecidingMovement)
			{
				// Debug.Log(direction + "  " + next.direction);
				decisionTimer = Mathf.Clamp(decisionTimer - Time.deltaTime, 0, decisionTimer);
				footTimer = Mathf.Clamp(footTimer - Time.deltaTime, 0, footTimer);
				bodyTimer = Mathf.Clamp(bodyTimer - Time.deltaTime, 0, bodyTimer);
				handTimer = Mathf.Clamp(handTimer - Time.deltaTime, 0, handTimer);

				if (next.direction.y == 0)
					if (next.direction.x > 0)
					{
						if (handTimer <= 0)
						{
							leftHandPosition = Vector3.Lerp(leftHandPosition, curPoint.leftHand.position, handTime);
							leftHandRotation = Quaternion.Lerp(leftHandRotation, curPoint.leftHand.rotation, handTime);
							leftElbowPosition = Vector3.Lerp(leftElbowPosition, curPoint.leftElbow.position, handTime);

							rightHandPosition = Vector3.Lerp(rightHandPosition, next.point.rightHand.position, handTime);
							rightHandRotation = Quaternion.Lerp(rightHandRotation, next.point.rightHand.rotation, handTime);
							rightElbowPosition = Vector3.Lerp(rightElbowPosition, next.point.rightElbow.position, handTime);
						}
						if (footTimer <= 0)
						{
							leftFootPosition = Vector3.Lerp(leftFootPosition, curPoint.leftFoot.position, footTime);
							leftFootRotation = Quaternion.Lerp(leftFootRotation, curPoint.leftFoot.rotation, footTime);
							leftKneePosition = Vector3.Lerp(leftKneePosition, curPoint.leftKnee.position, footTime);
							
							rightFootPosition = Vector3.Lerp(rightFootPosition, next.point.rightFoot.position, footTime);
							rightFootRotation = Quaternion.Lerp(rightFootRotation, next.point.rightFoot.rotation, footTime);
							rightKneePosition = Vector3.Lerp(rightKneePosition, next.point.rightKnee.position, footTime);
						}
					}
					else
					{
						if (handTimer <= 0)
						{
							leftHandPosition = Vector3.Lerp(leftHandPosition, next.point.leftHand.position, handTime);
							leftHandRotation = Quaternion.Lerp(leftHandRotation, next.point.leftHand.rotation, handTime);
							leftElbowPosition = Vector3.Lerp(leftElbowPosition, next.point.leftElbow.position, handTime);

							rightHandPosition = Vector3.Lerp(rightHandPosition, curPoint.rightHand.position, handTime);
							rightHandRotation = Quaternion.Lerp(rightHandRotation, curPoint.rightHand.rotation, handTime);
							rightElbowPosition = Vector3.Lerp(rightElbowPosition, curPoint.rightElbow.position, handTime);
						}
						if (footTimer <= 0)
						{
							leftFootPosition = Vector3.Lerp(leftFootPosition, next.point.leftFoot.position, footTime);
							leftFootRotation = Quaternion.Lerp(leftFootRotation, next.point.leftFoot.rotation, footTime);
							leftKneePosition = Vector3.Lerp(leftKneePosition, next.point.leftKnee.position, footTime);

							rightFootPosition = Vector3.Lerp(rightFootPosition, curPoint.rightFoot.position, footTime);
							rightFootRotation = Quaternion.Lerp(rightFootRotation, curPoint.rightFoot.rotation, footTime);
							rightKneePosition = Vector3.Lerp(rightKneePosition, curPoint.rightKnee.position, footTime);
						}
					}
				else
					if (next.direction.x > 0)
					{
						if (handTimer <= 0)
						{
							leftHandPosition = Vector3.Lerp(leftHandPosition, curPoint.leftHand.position, handTime);
							leftHandRotation = Quaternion.Lerp(leftHandRotation, curPoint.leftHand.rotation, handTime);
							leftElbowPosition = Vector3.Lerp(leftElbowPosition, curPoint.leftElbow.position, handTime);

							rightHandPosition = Vector3.Lerp(rightHandPosition, next.point.rightHand.position, handTime);
							rightHandRotation = Quaternion.Lerp(rightHandRotation, next.point.rightHand.rotation, handTime);
							rightElbowPosition = Vector3.Lerp(rightElbowPosition, next.point.rightElbow.position, handTime);
						}
						if (footTimer <= 0)
						{
							leftFootPosition = Vector3.Lerp(leftFootPosition, next.point.leftFoot.position, footTime);
							leftFootRotation = Quaternion.Lerp(leftFootRotation, next.point.leftFoot.rotation, footTime);
							leftKneePosition = Vector3.Lerp(leftKneePosition, next.point.leftKnee.position, footTime);
							
							rightFootPosition = Vector3.Lerp(rightFootPosition, curPoint.rightFoot.position, footTime);
							rightFootRotation = Quaternion.Lerp(rightFootRotation, curPoint.rightFoot.rotation, footTime);
							rightKneePosition = Vector3.Lerp(rightKneePosition, curPoint.rightKnee.position, footTime);
						}
					}
					else
					{
						if (next.direction.y < 0 && footTimer <= 0 || next.direction.y > 0 && handTimer <= 0)
						{
							leftHandPosition = Vector3.Lerp(leftHandPosition, next.point.leftHand.position, handTime);
							leftHandRotation = Quaternion.Lerp(leftHandRotation, next.point.leftHand.rotation, handTime);
							leftElbowPosition = Vector3.Lerp(leftElbowPosition, next.point.leftElbow.position, handTime);

							rightHandPosition = Vector3.Lerp(rightHandPosition, curPoint.rightHand.position, handTime);
							rightHandRotation = Quaternion.Lerp(rightHandRotation, curPoint.rightHand.rotation, handTime);
							rightElbowPosition = Vector3.Lerp(rightElbowPosition, curPoint.rightElbow.position, handTime);
						}
						if (next.direction.y < 0 && handTimer <= 0 || next.direction.y > 0 && footTimer <= 0)
						{
							leftFootPosition = Vector3.Lerp(leftFootPosition, curPoint.leftFoot.position, footTime);
							leftFootRotation = Quaternion.Lerp(leftFootRotation, curPoint.leftFoot.rotation, footTime);
							leftKneePosition = Vector3.Lerp(leftKneePosition, curPoint.leftKnee.position, footTime);

							rightFootPosition = Vector3.Lerp(rightFootPosition, next.point.rightFoot.position, footTime);
							rightFootRotation = Quaternion.Lerp(rightFootRotation, next.point.rightFoot.rotation, footTime);
							rightKneePosition = Vector3.Lerp(rightKneePosition, next.point.rightKnee.position, footTime);
						}
					}

				if (bodyTimer <= 0)
				{
					bodyPosition = Vector3.Lerp(bodyPosition, Vector3.Lerp(curPoint.body.position, next.point.body.position, bodyPercentage), bodyTime);
					// bodyRotation = Quaternion.Lerp(bodyRotation, Quaternion.Lerp(curPoint.body.rotation, next.point.body.rotation, .5f), bodyTime);
					var rotation = Quaternion.Euler(next.point.body.rotation.eulerAngles + Quaternion.Euler(0f, 0f, bodyExtraAngle * System.Math.Sign(-next.direction.x)).eulerAngles);
					bodyRotation = Quaternion.Lerp(bodyRotation, Quaternion.Lerp(curPoint.body.rotation, rotation, .5f), bodyTime);
				
					transform.parent.position = Vector3.Lerp(
						transform.parent.position,
						Vector3.Lerp(
							curPoint.transform.position - curPoint.transform.rotation * transformOffset,
							next.point.transform.position - next.point.transform.rotation * transformOffset, .5f),
						bodyTime);
					
					transform.parent.forward = Vector3.Lerp(
						transform.parent.forward,
						Vector3.Lerp(curPoint.transform.forward, next.point.transform.forward, .5f),
						bodyTime);
				}

				animator.SetIKLimb(AvatarIKGoal.LeftHand, curWeight, leftHandPosition, leftHandRotation, leftElbowPosition);
				animator.SetIKLimb(AvatarIKGoal.LeftFoot, curWeight, leftFootPosition, leftFootRotation, leftKneePosition);
				animator.SetIKLimb(AvatarIKGoal.RightHand, curWeight, rightHandPosition, rightHandRotation, rightElbowPosition);
				animator.SetIKLimb(AvatarIKGoal.RightFoot, curWeight, rightFootPosition, rightFootRotation, rightKneePosition);

				animator.bodyPosition = bodyPosition;
				animator.bodyRotation = bodyRotation;

				animator.SetLookAtWeight(curWeight);
				lookAtPosition = Vector3.Lerp(lookAtPosition, next.point.transform.position, headTime);
				animator.SetLookAtPosition(lookAtPosition);

				var nextState = State.None;
				if (direction == next.direction || next.transitionStyle == ClimbPointNeighbour.TransitionStyle.Automatic)
				{
					nextState = State.MovingToNext;
				}
				else if (direction == -next.direction)
				{
				 	nextState = State.MovingToPrevious;
				}

				var canChange = decisionTimer <= 0;
				if (canChange && (nextState != State.None))
				{
					state = nextState;
					footTimer = footDelay;
					bodyTimer = bodyDelay;
					handTimer = handDelay;
				}
			}
			else if (state == State.Dismounting)
			{
				// animator.applyRootMotion = true;
				// transform.parent.position += animator.deltaPosition / Time.deltaTime;
				transform.parent.position = Vector3.Lerp(transform.parent.position, next.point.transform.position, Time.deltaTime * 4);
				if (endDismount)
				{
					animator.SetBool("isClimbing", false);
					endDismount = false;
					state = State.None;
					animator.applyRootMotion = false;
					transform.parent.position = next.point.transform.position + Vector3.up * .1f;
					curPoint = null;
				}
			}
			// else 
			if (state == State.MovingToNext)
			{
				curPoint = next.point;
				next = null;
				state = State.Idle;
				decisionTimer = decisionStateDuration;
			}
			else if (state == State.MovingToPrevious)
			{
				state = State.Idle;
				decisionTimer = decisionStateDuration;
			}
		}
	} // end of class
	public static class AnimatorExtensions
	{
		public static void SetIKLimb(this Animator animator, AvatarIKGoal goal,
			float weight, Vector3 position, Quaternion rotation, Vector3 hintPosition)
		{
			animator.SetIKLimb(goal, weight, position, weight, rotation, weight, hintPosition);
		}

		public static void SetIKLimb(this Animator animator, AvatarIKGoal goal,
			float positionWeight, Vector3 position, float rotationWeight, Quaternion rotation, float hintPositionWeight, Vector3 hintPosition)
		{
			if (goal == AvatarIKGoal.LeftHand)
			{
				// ik position
				animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, positionWeight);
				animator.SetIKPosition(AvatarIKGoal.LeftHand, position);
				// ik rotation
				animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, rotationWeight);
				animator.SetIKRotation(AvatarIKGoal.LeftHand, rotation);
				// ik hint position
				animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, hintPositionWeight);
				animator.SetIKHintPosition(AvatarIKHint.LeftElbow, hintPosition);
			}
			else if (goal == AvatarIKGoal.RightHand)
			{
				// ik position
				animator.SetIKPositionWeight(AvatarIKGoal.RightHand, positionWeight);
				animator.SetIKPosition(AvatarIKGoal.RightHand, position);
				// ik rotation
				animator.SetIKRotationWeight(AvatarIKGoal.RightHand, rotationWeight);
				animator.SetIKRotation(AvatarIKGoal.RightHand, rotation);
				// ik hint position
				animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, hintPositionWeight);
				animator.SetIKHintPosition(AvatarIKHint.RightElbow, hintPosition);
			}
			else if (goal == AvatarIKGoal.LeftFoot)
			{
				// ik position
				animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, positionWeight);
				animator.SetIKPosition(AvatarIKGoal.LeftFoot, position);
				// ik rotation
				animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, rotationWeight);
				animator.SetIKRotation(AvatarIKGoal.LeftFoot, rotation);
				// ik hint position
				animator.SetIKHintPositionWeight(AvatarIKHint.LeftKnee, hintPositionWeight);
				animator.SetIKHintPosition(AvatarIKHint.LeftKnee, hintPosition);
			}
			else if (goal == AvatarIKGoal.RightFoot)
			{
				// ik position
				animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, positionWeight);
				animator.SetIKPosition(AvatarIKGoal.RightFoot, position);
				// ik rotation
				animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, rotationWeight);
				animator.SetIKRotation(AvatarIKGoal.RightFoot, rotation);
				// ik hint position
				animator.SetIKHintPositionWeight(AvatarIKHint.RightKnee, hintPositionWeight);
				animator.SetIKHintPosition(AvatarIKHint.RightKnee, hintPosition);
			}
		}
	} // end of class
} // end of namespace