﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Character : MonoBehaviour
	{
		public float speed = 4f;
		public float jumpSpeed = 4f;
		public Climbing climbing { get; private set; }
		Animator animator;
		new Rigidbody rigidbody;
		float timer;

		private void Awake()
		{
			climbing = GetComponentInChildren<Climbing>();
			animator = GetComponentInChildren<Animator>();
			rigidbody = GetComponent<Rigidbody>();
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				if (climbing.state == Climbing.State.None)
				{
					climbing.Begin();
					if (climbing.state != Climbing.State.FoundPoint)
					{
						timer = .2f;
						rigidbody.velocity = new Vector3(rigidbody.velocity.x, jumpSpeed, rigidbody.velocity.z);
						rigidbody.useGravity = true;
					}
					else
					{
						rigidbody.useGravity = false;
						rigidbody.velocity = Vector3.zero;
					}
				}
				else
				{
					rigidbody.useGravity = true;
					climbing.Drop();
				}
			}
		}

		private void FixedUpdate()
		{
			if (climbing.state == Climbing.State.None)
			{
				rigidbody.isKinematic = false;

				var direction = (CameraManager.instance.transform.right * Input.GetAxisRaw("Horizontal") +
					CameraManager.instance.transform.forward * Input.GetAxisRaw("Vertical")).normalized;
				
				// if (animator.GetBool("isLanding"))
				// 	direction = Vector3.zero;
				
				RaycastHit hit;
				bool isOnGround = false;
				if (timer <= 0)
				{
					if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 1.1f))
					{
						Debug.DrawRay(transform.position + Vector3.up, Vector3.down, Color.red);
						rigidbody.useGravity = false;
						isOnGround = true;
						rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0f, rigidbody.velocity.z);
						transform.position = hit.point;
					}
					else
					{
						rigidbody.useGravity = true;
						rigidbody.AddForce(Physics.gravity * 2);
					}
				}
				else
				{
					timer = Mathf.Clamp(timer - Time.deltaTime, 0f, timer);
				}

				var newVelocity = direction * speed;
				rigidbody.velocity = new Vector3(newVelocity.x, rigidbody.velocity.y, newVelocity.z);

				if (direction != Vector3.zero)
					transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 12f);
				
				animator.SetFloat("vertical", direction.magnitude, 0.1f, Time.deltaTime);
				animator.SetBool("isOnGround", isOnGround);
				animator.SetBool("isClimbing", false);
			}
			else
			{
				rigidbody.isKinematic = true;

				animator.SetFloat("vertical", 0f, 0.1f, Time.deltaTime);
				animator.SetBool("isOnGround", false);
				animator.SetBool("isClimbing", true);
			}
		}
	} // end of class
} // end of namespace