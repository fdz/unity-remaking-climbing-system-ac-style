﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class CameraManager : MonoBehaviour
	{
		public static CameraManager instance { get; private set; }

		public Transform target;
		public float followSpeed = 9;
		public float mouseSpeed = 2;
		public float controllerSpeed = 5;
		public float minAngle = -35;
		public float maxAngle = 35;
		public float lookAngle;
		public float tiltAngle;

		public bool lockon;
		public Transform lockonTransform;

		[HideInInspector] public Transform pivot;
		[HideInInspector] public Transform camTrans;

		float turnSmoothing = .1f;

		float smoothX;
		float smoothY;
		float smoothXVelocity;
		float smoothYVelocity;

		bool usedRightAxis;

		private void Awake()
		{
			CameraManager.instance = this;
			pivot = transform.GetChild(0);
			camTrans = pivot.transform.GetChild(0);
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}

		private void FixedUpdate()
		{
			if (Input.GetKeyDown(KeyCode.Mouse1))
			{
				Cursor.lockState = CursorLockMode.Locked;
			}

			float h = Input.GetAxis("Mouse X");
			float v = Input.GetAxis("Mouse Y");

			// float c_h = Input.GetAxis("RightAxis X");
			// float c_v = Input.GetAxis("RightAxis Y");

			float c_h = 0f;
			float c_v = 0f;

			float targetSpeed = mouseSpeed;

			if (lockonTransform != null)
			{
				if (Mathf.Abs(c_h) >.6f)
				{
					if (!usedRightAxis)
					{
						usedRightAxis = true;
					}
				}
			}

			if (usedRightAxis)
			{
				if (Mathf.Abs(c_h) < .6f)
				{
					usedRightAxis = false;
				}
			}

			if (c_h != 0 || c_v != 0)
			{
				h = c_h;
				v = c_v;

				targetSpeed = controllerSpeed;
			}

			FollowTarget();
			HandleRotations(v, h, targetSpeed);
		}

		void HandleRotations(float v, float h, float targetSpeed)
		{
			if (turnSmoothing > 0)
			{
				smoothX = Mathf.SmoothDamp(smoothX, h, ref smoothXVelocity, turnSmoothing);
				smoothY = Mathf.SmoothDamp(smoothY, v, ref smoothYVelocity, turnSmoothing);
			}
			else
			{
				smoothX = h;
				smoothY = v;
			}

			tiltAngle -= smoothY * targetSpeed;
			tiltAngle = Mathf.Clamp(tiltAngle, minAngle, maxAngle);
			pivot.localRotation = Quaternion.Euler(tiltAngle, 0, 0); //rotate camera on X axis

			if (lockonTransform == false)
				lockon = false;

			if (lockon && lockonTransform != null)
			{
				Vector3 targetDir = lockonTransform.position - transform.position;
				targetDir.Normalize();
				//targetDir.y = 0;

				if (targetDir == Vector3.zero)
				{
					targetDir = transform.forward;
				}
				Quaternion targetRot = Quaternion.LookRotation(targetDir);
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 9);
				lookAngle = transform.rotation.eulerAngles.y;
				return;
			}

			lookAngle += smoothX * targetSpeed;
			transform.rotation = Quaternion.Euler(0, lookAngle, 0); //rotate camera on Y axis
		}

		void FollowTarget()
		{
			float speed = Time.deltaTime * followSpeed;
			Vector3 targetPosition = Vector3.Lerp(transform.position, target.position, speed);
			transform.position = targetPosition;
		}
	}
}